import React from "react"
import { Link } from "gatsby"

import "./post-link.scss"

const PostLink = ({ post }) => {
  return (
    <Link to={post.frontmatter.slug} state={{ post }}>
      <div className="post">
        <div className="bar">
          <span className="dots">
            <span className="dot close"></span>
            <span className="dot expand"></span>
            <span className="dot minimize"></span>
          </span>
        </div>
        <div className="content">
          <div className="title">
            Quoi tu es passioné.e et {post.frontmatter.title}
          </div>
          <div className="author">@Anonyme.</div>
        </div>
      </div>
    </Link>
  )
}
export default PostLink
