import React, { Fragment } from "react"
import { Link } from "gatsby"

import { Row, Col } from "antd"

function Header() {
  return (
    <Link to="/">
      <header> <img alt="Heart breathing" src="/heart.png" /> lapassion</header>
    </Link>
  )
}

export default function Layout({ children }) {
  return (
    <Fragment>
      <Header></Header>
      <Row className="content" justify="center">
        <Col xs={23} sm={22} md={20} lg={22} xl={22} xxl={16}>
          <Row justify="center">{children}</Row>
        </Col>
      </Row>
    </Fragment>
  )
}
