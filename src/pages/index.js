import React from "react"
import { graphql, Link } from "gatsby"
import PostLink from "../components/post-link"

import "./index.scss"
import Layout from "../components/layout"

const IndexPage = ({
  data: {
    allMarkdownRemark: { edges },
  },
}) => {
  const Posts = edges
    .filter(edge => !!edge.node.frontmatter.date) // You can filter your posts based on some criteria
    .map(edge => <PostLink key={edge.node.id} post={edge.node} />)

  return (
    <Layout>
      <div className="home">
        {Posts.map((post, index) => {
          const appearDuration = Math.random() * 1.5 + 0.5
          return (
            <div
              key={index}
              style={{
                animationName: "appear",
                animationDuration: `${appearDuration}s`,
                animationTimingFunction: "ease"
              }}
            >
              {post}
            </div>
          )
        })}
      </div>
    </Layout>
  )
}

export default IndexPage

export const GoHome = () => {
  return (
    <Link
      style={{
        color: "white",
      }}
      to="/"
    >
      > Go home
    </Link>
  )
}

export const pageQuery = graphql`
  query {
    allMarkdownRemark(sort: { order: DESC, fields: [frontmatter___date] }) {
      edges {
        node {
          id
          excerpt(pruneLength: 250)
          frontmatter {
            date(formatString: "MMMM DD, YYYY")
            slug
            title
          }
        }
      }
    }
  }
`
