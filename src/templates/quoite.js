import React from "react"
import { graphql } from "gatsby"
import PostLink from "../components/post-link"
import Layout from "../components/layout"
import { GoHome } from "../pages"

const QuoiteTemplate = ({ data, pageContext, location }) => {
  const { markdownRemark } = data // data.markdownRemark holds your post data

  return (
    <Layout>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
        }}
      >
        <PostLink post={markdownRemark}></PostLink>
        <GoHome />
      </div>
    </Layout>
  )
}
export default QuoiteTemplate

export const pageQuery = graphql`
  query($slug: String!) {
    markdownRemark(frontmatter: { slug: { eq: $slug } }) {
      html
      frontmatter {
        date(formatString: "MMMM DD, YYYY")
        slug
        title
      }
    }
  }
`
